﻿using UnityEngine;
using System.Collections;
using Kinect;
public class PersonageController : MonoBehaviour {
    public Transform[] lanes;
    int current = 2;
    Animator anim;
    public SkeletonWrapper sw;
    bool transition = false;
    bool dead = false;
    ObstaclesController obsContr;
    public FailureController fc;
    int lives = 5;
    public bool[] checkedLanes;
    GameParams gameParams = new GameParams();

    bool firstLaunch = true;
    public GameObject countDown;
    float previousR = 0f;
    void Start()
    {
        gameParams = gameParams.Load();
        lives = gameParams.lives;
        obsContr = GameObject.FindObjectOfType<ObstaclesController>();
        obsContr.changeLaneDelay = gameParams.gap;
        GameController.Instance.GameTime = gameParams.gameTime;
        anim = this.GetComponent<Animator>();
        checkedLanes = new bool[lanes.Length];
        checkedLanes[2] = true;
    }

    // Update is called once per frame
    void Update()
    {
        TotalLivesShow.Instance.SetLives(lives);
        GameSessionController.Instance.gameSession.finalLives = lives;

        if (firstLaunch)
        {
            if (sw.pollSkeleton() && sw.trackedCount > 0)
                    countDown.SetActive(true);
            return;
        }


        if (dead || transition)
            return;

        if (Input.GetKeyDown(KeyCode.A) && current > 0)
        {
            anim.SetTrigger("Left");
            transition = true;
            MusicController.Instance.ChangeLane();
        } else 
        if (Input.GetKeyDown(KeyCode.D) && current < lanes.Length - 1)
        {
            anim.SetTrigger("Right");
            transition = true;
            MusicController.Instance.ChangeLane();
        }
        if (Input.GetKey(KeyCode.W))
            obsContr.speedMultiply =2f;
        if (sw.pollSkeleton())
        {
            KinectPlayerController();
        }
    }

    public void KinectPlayerController()
    {
        float r = Rotation();
        if (r > 0.13)
            r = 1;
        else
            if (r < -0.13)
                r = -1;
            else r = 0;

        if (obsContr != null && obsContr.speedMultiply > 0)
        {
            if (r > 0f && current < lanes.Length - 1 && !transition && previousR != r)
            {
                anim.SetTrigger("Right");
                transition = true;
                MusicController.Instance.ChangeLane();
            }
            else
                if (r < 0f && current > 0 && !transition && previousR != r)
                {
                    anim.SetTrigger("Left");
                    transition = true;
                    MusicController.Instance.ChangeLane();
                }
        }
        if (obsContr != null)
        {
            if (isSitting())
            {

                obsContr.speedMultiply += 0.05f;
            }
            else
                obsContr.speedMultiply -= 0.01f;

            obsContr.speedMultiply = Mathf.Clamp(obsContr.speedMultiply, 0f, 2f);
        }
        previousR = r;
    }

    public void EndCountDown()
    {
        firstLaunch = false;
    }

    public void Dead()
    {
        lives--;
        GameSessionController.Instance.gameSession.finalLives = lives;
        TotalLivesShow.Instance.SetLives(lives);
        if (lives <=0)
            Application.Quit();
        for (int i = 0; i < checkedLanes.Length; i++)
            if (checkedLanes[i])
            current = i;
        this.transform.position = lanes[current].position;
        dead = false;
    }

    public void MoveRight()
    {
        current++;
        this.transform.position = lanes[current].position;
        transition = false;
    }

    public void MoveLeft()
    {
        current--;
        this.transform.position = lanes[current].position;
        transition = false;
    }

    public float Rotation()
    {
             return (sw.bonePos[0, (int)Kinect.NuiSkeletonPositionIndex.ShoulderLeft] - sw.bonePos[0, (int)Kinect.NuiSkeletonPositionIndex.ShoulderRight]).z;
    }

    public bool isSitting()
    {
           
            Vector3 spineBase = sw.bonePos[0, (int)Kinect.NuiSkeletonPositionIndex.Spine];
            Vector3 spineShoulder = sw.bonePos[0, (int)Kinect.NuiSkeletonPositionIndex.ShoulderCenter];
            //Debug.Log((spineBase - spineShoulder).z);
            return (spineBase - spineShoulder).z < -0.03f;
    }

    void OnTriggerEnter(Collider col)
    {
        if (!transition)
        {
            obsContr.speedMultiply = 0f;
            anim.SetTrigger("Dead");
            MusicController.Instance.Fall();
            dead = true;
            fc.Failure(1f);
        }
    }
}
