﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour
{
    private static MusicController instance_;
    public static MusicController Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<MusicController>();
            return instance_;
        }
    }
    AudioSource music;
    AudioSource sounds;
    public AudioClip clip0;
    public AudioClip clip1;
    void Start()
    {
        music = GetComponents<AudioSource>()[0];
        sounds = GetComponents<AudioSource>()[1];
    }

    public void Fall()
    {
        sounds.clip = clip0;
        sounds.Play();
    }

    public void ChangeLane()
    {
        sounds.clip = clip1;
        sounds.Play();
    }
}
