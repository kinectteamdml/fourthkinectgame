﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrefabsPool : MonoBehaviour {
    private static PrefabsPool instance_;
    public static PrefabsPool Instance {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<PrefabsPool>();
            return instance_;
        }
    }

    public GameObject[] prefabs;
    List<Stack<GameObject>> pools;
    public List<Obstacle> obstacles = new List<Obstacle>();
	void Start () {
     
        pools = new List<Stack<GameObject>>();
        for (int i = 0; i < prefabs.Length; i++)
            pools.Add(new Stack<GameObject>());
        
	}
	
    public int GetRandomId()
    {
        return Random.Range(0, prefabs.Length);
    }
    public GameObject GetFromPool(int num)
    {
        if (pools[num].Count > 0)
        {
            GameObject obj = pools[num].Pop();
            obstacles.Add(obj.GetComponent<Obstacle>());
            obj.SetActive(true);
            return obj;
        }
        else {
            GameObject obj = Instantiate(prefabs[num]);
            Obstacle obstcle = obj.GetComponent<Obstacle>();
            obstcle.id = num;
            obstacles.Add(obstcle);
            obj.SetActive(true);
            obj.transform.SetParent(transform);
            return obj;
        }

    }

    public void PutToPool(GameObject go)
    {
        Obstacle obstacle = go.GetComponent<Obstacle>();
        if (obstacle==null)
            return;
        pools[obstacle.id].Push(go);
        obstacles.Remove(obstacle);
        go.SetActive(false);
    }

}
