﻿using UnityEngine;
using System.Collections;

public class RoadsController : MonoBehaviour {

    ObstaclesController obstaclesController;
    Vector3[] positions;
    Transform last;
	void Start () {
        obstaclesController = GameObject.FindObjectOfType<ObstaclesController>();
	    positions = new Vector3[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            last = transform.GetChild(i);
            positions[i] = transform.GetChild(i).position;
        }
       
    }
	
	// Update is called once per frame
	void Update () {
        foreach (Transform t in transform)
        {
            t.position += obstaclesController.speed*obstaclesController.speedMultiply * Vector3.back * Time.deltaTime;
        }

        foreach (Transform t in transform)
            if (t.position.z < positions[0].z)
        {
            t.position = last.transform.position + Vector3.forward * 22.5f;
            last = t;
        }
	}
}
